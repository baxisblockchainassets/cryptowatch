Codes to extract data from Cryptowatch
	- Price
	- OrderBooks
	- Exchanges

Information about Cryptowatch data: https://baxisblockchainassets.atlassian.net/wiki/spaces/DE/pages/22512592/DCM%2B-%2BCryptowatch
## Access to Binder
	
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Fbaxisblockchainassets%2Fcryptowatch%2Fsrc%2Fmaster/master)

## Requirements

	pip install schedule
	pip install requests
	pip install websocket
## Web API
	- API REST
	- WebSocket